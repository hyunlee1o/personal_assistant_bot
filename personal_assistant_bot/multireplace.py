import re


def generate_normalize(ignore_case, multiline):
    if ignore_case:

        def normalize_old(s):
            return s.lower()

        re_mode = re.IGNORECASE

    else:

        def normalize_old(s):
            return s

        re_mode = 0
    if multiline:
        re_mode |= re.M
    return normalize_old, re_mode


def generate_pattern(replacements, ignore_case, re_mode, verbose=True):
    # If case insensitive, we need to normalize the old string so that later a replacement
    # can be found. For instance with {"HEY": "lol"} we should match and find a replacement for "hey",
    # "HEY", "hEy", etc.

    # Place longer ones first to keep shorter substrings from matching where the longer ones should take place
    # For instance given the replacements {'ab': 'AB', 'abc': 'ABC'} against the string 'hey abc', it should produce
    # 'hey ABC' and not 'hey ABc'
    def fix_whitespaces(item):
        return item.replace("\\ ", " ")

    rep_sorted = sorted(replacements, key=len, reverse=True)
    rep_escaped = map(re.escape, rep_sorted)
    rep_escaped = map(fix_whitespaces, rep_escaped)
    # rep_separated = [rf"{word}" for word in rep_escaped]
    # Create a big OR regex that matches any of the substrings to replace
    # We make sure dots don't collide with urls and that words separated by commas don't break, also spaces and end of string
    pattern = re.compile(
        rf"(?<=\b)({'|'.join(rep_escaped)})(?=\.($|\s)|$|\,|\s)", re_mode
    )
    return pattern


def multireplace(string, replacements, ignore_case=False, multiline=False):
    """
    Given a string and a replacement map, it returns the replaced string.
    :param str string: string to execute replacements on
    :param dict replacements: replacement dictionary {value to find: value to replace}
    :param bool ignore_case: whether the match should be case insensitive
    :rtype: str
    """
    normalize_old, re_mode = generate_normalize(ignore_case, multiline)
    replacements_norm = {normalize_old(key): val for key, val in replacements.items()}
    pattern = generate_pattern(replacements_norm, ignore_case, re_mode)
    # For each match, look up the new string in the replacements, being the key the normalized old string
    return pattern.sub(
        lambda match: replacements_norm[normalize_old(match.group(0))], string
    )
