#!/usr/bin/env python3
import random
import os
import time
import asyncio
import logging
import subprocess
import sys
import json
from logging.handlers import RotatingFileHandler
import requests
from dotenv import load_dotenv
from telethon.sync import TelegramClient
from telethon import events
from utils import get_user_from_event, get_user_from_id, MUTE_RIGHTS
from telethon.errors import (
    UserIdInvalidError,
    UserAdminInvalidError,
    ChatAdminRequiredError,
    BadRequestError,
)
from telethon.tl.functions.users import GetFullUserRequest
from telethon.tl.functions.channels import GetParticipantRequest, EditBannedRequest
from telethon.tl.functions.messages import GetCommonChatsRequest
import typing
import multireplace

load_dotenv()
local_dir = os.path.dirname(os.path.realpath(__file__))

logger = logging.getLogger()
logname = f"{local_dir}/logs/personal_assistant_bot.log"
FORMAT = "[%(levelname) 5s/%(asctime)s] %(name)s: %(message)s"
logging.basicConfig(
    handlers=[RotatingFileHandler(logname, maxBytes=100_000, backupCount=5)],
    level=logging.WARNING,
    format=FORMAT,
    datefmt="%Y-%m-%dT%H:%M:%S",
)
api_id = int(os.environ["API_ID"])
api_hash = os.environ["API_HASH"]
pythonista_network_handles = [
    "@python",
    "@pythonofftopic",
    "@django",
    "@SeleniumPython",
]

with open(os.path.join(local_dir, "replace.json")) as json_data:
    replaces = json.load(json_data)
words = sorted(replaces.keys(), key=len, reverse=True)
# match_string = [rf"{word}" for word in words]
joined_words = "|".join(words)
multireplace_pattern = rf"(?ism).*({joined_words})"

with TelegramClient("name", api_id, api_hash) as client:
    print("started")
    logger.info("Started")

    # pythonista_network_channels = asyncio.get_event_loop().run_until_complete(
    #     load_pythonista(pythonista_network_handles)
    # )
    @client.on(
        events.NewMessage(
            outgoing=True, pattern="\!{2}stop|\!{2}quit|\!{2}exit", forwards=False
        )
    )
    async def handler(event):
        await event.delete()
        logger.info("Stopping bot")
        sys.exit(0)

    @client.on(events.NewMessage(outgoing=True, pattern="\!{2}restart", forwards=False))
    async def handler(event):
        await event.delete()
        python = sys.executable
        os.execl(python, python, *sys.argv)
        logger.info("Restarting")
        sys.exit(0)

    @client.on(events.NewMessage(outgoing=True, pattern="!{2}user", forwards=False))
    async def handler(event):
        message = event.message.message.split()
        if event.is_reply:
            reply = await event.message.get_reply_message()
            entity_id = reply.sender_id
        else:
            if len(message) == 1:
                entity = await client.get_me()
                entity_id = entity.id
            elif "@" in message[1]:
                entity_id = message[1]
            elif message[1].isnumeric():
                entity_id = int(message[1])
            else:
                logger.info(f"Invalid message: {message}")
                await event.delete()
                return
        try:
            entity = await client.get_entity(entity_id)
        except Exception as err:
            logger.error(f"Unknown or invalid user: {entity}")
            await event.edit(str(err))
            return None
        entity_dict = entity.to_dict()
        values = [
            "username",
            "id",
            "first_name",
            "last_name",
            "bot",
            "restricted",
            "restriction_reason",
            "scam",
            "deleted",
        ]
        ans = "\n".join(
            [
                f"<b>{value.replace('_',' ')}</b>: <code>{entity_dict.get(value)}</code>"
                for value in values
            ]
        )
        await event.respond(ans, parse_mode="html")

    @client.on(events.NewMessage(outgoing=True, pattern="\!{2}id", forwards=False))
    async def handler(event):
        if event.is_reply:
            reply_msg = await event.get_reply_message()
            user = await client.get_entity(reply_msg.from_id)
        elif "@" in event.message.message:
            message = event.message.message.split()
            try:
                full = await client(GetFullUserRequest(message[1]))
            except Exception:
                logger.error(
                    f"Invalid message or user not existent: {event.message.message}"
                )
            user = full.user
        await event.delete()
        user_msg = f"The user you requested: <a href=tg://user?id={user.id}>{user.first_name}</a> aka id: {user.id}"
        await client.send_message("me", user_msg, parse_mode="html")

    @client.on(
        events.NewMessage(
            outgoing=True, pattern="\!{2}purge|\!{2}flash", forwards=False
        )
    )
    async def handler(event):
        if event.is_reply:
            reply_msg = await event.get_reply_message()
        else:
            return
        id_channel = await event.get_input_chat()
        async for message in client.iter_messages(
            id_channel, min_id=reply_msg.id, reverse=True
        ):
            await message.delete()
        logger.info("Purge operation")

    @client.on(events.NewMessage(outgoing=True, pattern="!{2}ping", forwards=False))
    async def handler(event):
        s = time.time()
        message = await event.reply("Pong!")
        d = time.time() - s
        reply_msg = f"Pong! __(reply took {d:.3f}s)__"
        await message.edit(reply_msg)
        logger.info(reply_msg)
        await asyncio.sleep(5)
        await asyncio.wait([event.delete(), message.delete()])
        # DeprecationWarning: The explicit passing of coroutine objects to asyncio.wait() is deprecated since Python 3.8, and scheduled for removal in Python 3.11.

    @client.on(events.NewMessage(outgoing=True, pattern=multireplace_pattern))
    async def handler(event):
        def can_write(member):
            return member.channel.default_banned_rights.send_messages

        def is_admin(member):
            return member.channel.admin_rights

        if not event.is_private:
            id_channel = await event.get_input_chat()
            group_entity = await client.get_entity(id_channel)
            me = GetParticipantRequest(group_entity, "me")
            python_admin_bot = GetParticipantRequest(
                group_entity, "@Doragonsureiya_bot"
            )
            if (
                is_admin(me)
                or is_admin(python_admin_bot)
                or can_write(python_admin_bot)
            ):
                new_msg = multireplace.multireplace(
                    event.message.message, replaces, ignore_case=True, multiline=True
                )
                await event.message.edit(new_msg, parse_mode="html")
        else:
            user = await client.get_entity(event.chat_id)
            pythonista_entities = await client.get_entity(pythonista_network_handles)
            pythonista_entities_id = [entity.id for entity in pythonista_entities]
            common_groups = await client(
                GetCommonChatsRequest(user_id=user.id, max_id=0, limit=100)
            )
            # for group in entities:
            #     try:
            #         is_inside = await client(GetParticipantRequest(group, user.id))
            #         print(is_inside)
            #     except Exception as e:
            #         print(e)
            #         traceback.print_exc()

            # means that the common groups that i have with the user are inside the pythonista network
            if any(
                [
                    group.id
                    for group in common_groups.chats
                    if group.id in pythonista_entities_id
                ]
            ):
                new_msg = multireplace.multireplace(
                    event.message.message, replaces, ignore_case=True, multiline=True
                )
                await event.message.edit(new_msg, parse_mode="html")

    @client.on(events.NewMessage(outgoing=True, pattern=r"\!{2}s"))
    async def handler(event):
        reply_msg = await event.get_reply_message()
        if event.is_reply and len(event.message.message.split()) == 1:
            to_sarcasm = reply_msg.message.split()
        else:
            to_sarcasm = event.message.message.split()[1:]

        def sarcasmify(text):
            def sarcasmify_(word):
                return "".join(
                    [
                        letter.upper() if random.randint(0, 1) else letter.lower()
                        for letter in word
                    ]
                )

            return " ".join([sarcasmify_(word) for word in text])

        logger.info("sarcasmify completed")
        await event.message.edit(sarcasmify(to_sarcasm), parse_mode="html")

    @client.on(events.NewMessage(outgoing=True, pattern=r"\!{2}run"))
    async def handler(event):
        LIMIT = 4096
        args = event.message.message.split()
        if len(args) < 2:
            message = await event.respond("something went wrong")
            await asyncio.sleep(5)
            await asyncio.wait([event.delete(), message.delete()])
            return
        logger.info(f"Requested command :{' '.join(args[1:])}")
        try:
            out = subprocess.run(
                args[1:], capture_output=True, timeout=5, check=True, text=True
            ).stdout[:LIMIT]
        except Exception:
            error_msg = f"Invalid call: {args}"
            logger.error(error_msg)
            await event.message.edit(error_msg)
        message = f"<code>{out}</code>"
        await event.message.edit(message, parse_mode="html")

    @client.on(events.NewMessage(outgoing=True, pattern=r"\!{2}new"))
    async def handler(event):
        message = "If you are asking that question you probably haven't read the <b>PINNED MESSAGE</b> in the main group. You should be patient and read everything in there before asking further questions.</b> In the case you were using Telegram from Iran, Telegram (not @python admins) muted all Iran accounts in international public groups. Check this post for further information: <a href='https://t.me/tgbeta/3734'>https://t.me/tgbeta/3734</a>"
        await event.message.edit(message, parse_mode="html")

    @client.on(events.NewMessage(outgoing=True, pattern=r"\!{2}pyban"))
    async def handler(event):
        if event.is_reply:
            reply_msg = await event.get_reply_message()
            user = await client.get_entity(reply_msg.from_id)
            reason = "spam"
            args = event.message.message.split()
            if len(args) >= 2:
                reason = f"{''.join(args[1:])}"
            python_admins_channel_id = int(os.environ["PYTHON_ADMINS_CHANNEL_ID"])
            receiver = await client.get_entity(python_admins_channel_id)
            message = f"/ban {user.id} {reason}"
            await event.delete()
            await client.send_message(receiver, message, parse_mode="html")

    @client.on(events.NewMessage(outgoing=True, pattern=r"\!{2}pm"))
    async def handler(event):
        if event.is_reply:
            logger.info("Some fucked pm'ed me")
            reply_msg = await event.get_reply_message()
            user = await client.get_entity(reply_msg.from_id)
            user_msg = f"<a href=tg://user?id={user.id}>{user.first_name}</a> aka id: {user.id}"
        message = f"Please {user_msg} dont PM me. This is my private space, PLEASE read the \n<a href=t.me/pythonrules>RULES</a>                       <a href=t.me/pythonrules>RULES</a>                         <a href=t.me/pythonrules>RULES</a>\n                  <a href=t.me/pythonrules>RULES</a>                        <a href=t.me/pythonrules>RULES</a>\n<a href=t.me/pythonrules>RULES</a>                       <a href=t.me/pythonrules>RULES</a>                         <a href=t.me/pythonrules>RULES</a>\nand ask whatever you need in THE MAIN GROUP."
        # if len(args) == 2:
        #     receiver = args[1].split("@")[-1]
        #     message += f"@{receiver}."
        #     await client.send_message(receiver, message, parse_mode="html")
        # else:
        await event.message.edit(message, parse_mode="html")
        # await asyncio.sleep(5)

    @client.on(events.NewMessage(outgoing=True, pattern=r"\!{2}haste"))
    async def handler(event):
        reply_msg = await event.get_reply_message()
        if event.is_reply and reply_msg.message:
            user = await client.get_entity(reply_msg.from_id)
            user_msg = f"<a href=tg://user?id={user.id}>{user.first_name}</a>"
            if reply_msg.text[0] == "`" and reply_msg.text[-1] == "`":
                code = reply_msg.replace("`", "")
            else:
                code = reply_msg.text
            try:
                haste = requests.post(
                    "https://hastebin.com/documents", data=code, timeout=5
                ).json()["key"]
            except:
                await client.send_message("me", "Hastebin is ducked")
                logger.info("Hastebin is not working properly")
                await event.delete()
            await event.respond(
                f"{user_msg} tried to post <a href='http://hastebin.com/{haste}'><u>this code snippet<u></a>\nFor long posts use a pasting service.",
                parse_mode="html",
            )
            await event.delete()

    @client.on(events.NewMessage(outgoing=True, pattern=r"\!{2}lib"))
    async def handler(event):
        args = event.message.message.split()
        python_version = 3
        if len(args) < 2:
            message = await event.respond("something went wrong")
            await asyncio.sleep(5)
            await asyncio.wait([event.delete(), message.delete()])
            return
        lib = args[1]
        extra = f"#{args[2]}" if len(args) > 2 else ""
        url = f"https://docs.python.org/{python_version}/library/{lib}.html{extra}"
        if requests.get(url).status_code == 200:
            ans = f'<a href="{url}">Check out <b>{lib.upper()}</b> documentation</a>.\nI think it will be useful to you.'
        else:
            ans = f'<code>{lib}</code> library doesn\'t exist in the official python docs.\nCheck out <a href="https://docs.python.org/{python_version}"/library/index.html>THE OFFICIAL DOCS</a> to review the whole list of libraries.\n If you don\'t find it there try in <a href="https://github.com/vinta/awesome-python">AWESOME PYTHON</a>. \nThey are probably python bindings to a library written in another language or they have their own webpage.'

        await event.message.edit(ans, parse_mode="html")

    #!!video
    @client.on(events.NewMessage(pattern=r"\!{2}video"))
    async def handler(event):
        def get_file(conv):
            response = conv.get_response(events)
            while response.message.media is None:
                response = conv.get_response(events)
                if "cancel" in response.message.message:
                    return
            return response.message.media

        if event.is_private:
            user = await event.get_input_user()
            user_ent = await client.get_entity(user)
            async with client.conversation(user_ent) as conv:
                await client.send_message(
                    user, "Send the video or the audio you want to blend"
                )
                file1 = get_file(conv)
                if file1 is None:
                    client.send_message("Cancelling operation")
                    return
                await client.send_message(user, "Send the other file")
                file2 = get_file(conv)
                if file2 is None:
                    client.send_message("Cancelling operation")
                    return

    @client.on(events.NewMessage(pattern=r"\!{2}mute"))
    async def handler(event):
        """
        This function is basically muting peeps
        """
        # Admin or creator check
        chat = await event.get_chat()

        # If not admin and not creator, return
        if not chat.admin_rights and not chat.creator:
            await event.delete()
            return

        user = await get_user_from_event(event)
        if not user:
            return

        self_user = await event.client.get_me()
        if user.id == self_user.id:
            await event.edit("You can't mute yourself", delete_in=3)
            return

        # If everything goes well, do announcing and mute
        await event.edit(f"Muting `{user.id}`...")

        try:
            await event.client(EditBannedRequest(event.chat_id, user.id, MUTE_RIGHTS))
            # Announce that the function is done
            await event.edit(f"Muted `{user.id}`")
            logger.info(
                f"#MUTE\nUSER: [{user.first_name}](tg://user?id={user.id})\nCHAT: {event.chat.title}(`{event.chat_id}`)"
            )
        except UserIdInvalidError:
            return await event.edit("`Uh oh my unmute logic broke!`")

        # These indicate we couldn't hit him an API mute, possibly an
        # admin?
        except (UserAdminInvalidError, ChatAdminRequiredError, BadRequestError):
            return await event.edit(
                "I couldn't mute on the API, could be an admin possibly?"
            )

    @client.on(events.NewMessage(pattern=r"\!{2}kick|\!{2}yeet"))
    async def handler(event):
        # Admin or creator check
        chat = await event.get_chat()

        # If not admin and not creator, return
        if not chat.admin_rights and not chat.creator:
            await event.delete()
            return

        user = await get_user_from_event(event)
        if not user:
            return
        self_user = await event.client.get_me()
        if user.id == self_user.id:
            await event.edit("You can't kick yourself", delete_in=3)
            return
        try:
            await client.kick_participant(chat, user)
            await event.edit(f"Yeeted `{user.id}`")
        except Exception:
            await event.delete()
            logger.info(f"failed kicking user={user.id}")

    @client.on(events.NewMessage(pattern="(?i)!!(list|help)", forwards=False))
    async def handler(event):
        await event.delete()
        text = "Available commands:\n"
        for callback, handle in client.list_event_handlers():
            if isinstance(handle, events.NewMessage) and callback.__doc__:
                text += f"\n{callback.__doc__.strip()}"
        message = await event.respond(text, link_preview=False)
        await asyncio.sleep(1 * text.count(" "))  # Sleep ~1 second per word
        await message.delete()

    client.run_until_disconnected()
